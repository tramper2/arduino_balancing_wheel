#include "SelfMotor.h"
#include "Arduino.h"

AF_DCMotor LeftMotor(3);
AF_DCMotor RightMotor(4);

SelfMotor::SelfMotor(double motorAConst, double motorBConst)
{
    _motorAConst = motorAConst;
    _motorBConst = motorBConst;
}


void SelfMotor::move(int leftSpeed, int rightSpeed, int minAbsSpeed)
{
    if (rightSpeed < 0)
    {
        rightSpeed = min(rightSpeed, -1*minAbsSpeed);
        rightSpeed = max(rightSpeed, -255);
    }
    else if (rightSpeed > 0)
    {
        rightSpeed = max(rightSpeed, minAbsSpeed);
        rightSpeed = min(rightSpeed, 255);
    }
    
    int realRightSpeed = map(abs(rightSpeed), 0, 255, minAbsSpeed, 255);

    if (leftSpeed < 0)
    {
        leftSpeed = min(leftSpeed, -1*minAbsSpeed);
        leftSpeed = max(leftSpeed, -255);
    }
    else if (leftSpeed > 0)
    {
        leftSpeed = max(leftSpeed, minAbsSpeed);
        leftSpeed = min(leftSpeed, 255);
    }
    
    int realLeftSpeed = map(abs(leftSpeed), 0, 255, minAbsSpeed, 255);
    
    RightMotor.setSpeed(realRightSpeed * _motorAConst);
    LeftMotor.setSpeed(realLeftSpeed * _motorBConst);

    if(rightSpeed > 0) RightMotor.run(FORWARD);
    else  RightMotor.run(BACKWARD);
        
    if(leftSpeed > 0) LeftMotor.run(FORWARD);
    else LeftMotor.run(BACKWARD);

}


void SelfMotor::move(int speed, int minAbsSpeed)
{
    int direction = 1;
    
    if (speed < 0)
    {
        direction = -1;
        
        speed = min(speed, -1*minAbsSpeed);
        speed = max(speed, -255);
    }
    else
    {
        speed = max(speed, minAbsSpeed);
        speed = min(speed, 255);
    }
    
    if (speed == _currentSpeed) return;
    
    int realSpeed = max(minAbsSpeed, abs(speed));

    RightMotor.setSpeed(realSpeed * _motorAConst);
    LeftMotor.setSpeed(realSpeed * _motorBConst);

    if(speed > 0) RightMotor.run(FORWARD);
    else  RightMotor.run(BACKWARD);
        
    if(speed > 0) LeftMotor.run(FORWARD);
    else LeftMotor.run(BACKWARD);
    
    _currentSpeed = direction * realSpeed;
}


void SelfMotor::move(int speed)
{
    if (speed == _currentSpeed) return;
    
    if (speed > 255) speed = 255;
    else if (speed < -255) speed = -255;
    
    RightMotor.setSpeed(abs(speed) * _motorAConst);
    LeftMotor.setSpeed(abs(speed) * _motorBConst);

    if(speed > 0) RightMotor.run(FORWARD);
    else  RightMotor.run(BACKWARD);
        
    if(speed > 0) LeftMotor.run(FORWARD);
    else LeftMotor.run(BACKWARD);
   
    _currentSpeed = speed;
}


void SelfMotor::turnLeft(int speed, bool kick)
{
    LeftMotor.run(RELEASE);
    RightMotor.run(FORWARD);

    if (kick) //급출발인건가?
    {
        RightMotor.setSpeed(255);
        LeftMotor.setSpeed(255);
        delay(100);
    }
    
    RightMotor.setSpeed(speed * _motorAConst);
    LeftMotor.setSpeed(speed * _motorBConst);
}


void SelfMotor::turnRight(int speed, bool kick)
{
    LeftMotor.run(FORWARD);
    RightMotor.run(RELEASE);
 
    if (kick)
    {
        RightMotor.setSpeed(255);
        LeftMotor.setSpeed(255);
    
        delay(100);
    }
    
    RightMotor.setSpeed(speed * _motorAConst);
    LeftMotor.setSpeed(speed * _motorBConst);
}


void SelfMotor::stopMoving()
{
    LeftMotor.run(RELEASE);
    RightMotor.run(RELEASE);
    _currentSpeed = 0;
}