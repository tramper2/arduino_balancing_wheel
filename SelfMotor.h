#ifndef SelfMotor_h
#define SelfMotor_h


#include "Arduino.h"
#include <AFMotor.h>


class SelfMotor
{
protected:

    int _currentSpeed;
    double _motorAConst, _motorBConst;
public:
    SelfMotor(double motorAConst, double motorBConst);
    void move(int leftSpeed, int rightSpeed, int minAbsSpeed);
    void move(int speed);
    void move(int speed, int minAbsSpeed);
    void turnLeft(int speed, bool kick);
    void turnRight(int speed, bool kick);
    void stopMoving();
};


#endif