//참고할 셀플 발란싱 로봇
//https://github.com/kurimawxx00/arduino-self-balancing-robot/blob/master/AmBOT_final_nano.ino
//한국 https://blog.naver.com/gyurse/221037380951
    #include <PID_v1.h>
    #include <I2Cdev.h>
    #include <MPU6050_6Axis_MotionApps20.h>
    #include "SelfMotor.h"

    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
    #endif

    #define MIN_ABS_SPEED 80  //모터의 최저 속도
    #define OUTPUT_READABLE_YAWPITCHROLL    // Yaw, Pitch, Roll 값을 얻기 위해 선언합니다
    #define INTERRUPT_PIN 2
    MPU6050 mpu;

    // MPU 컨트롤/상태 변수
    bool dmpReady = false; // DMP가 성공한 경우 true로 설정
    uint8_t mpuIntStatus; // MPU의 실제 인터럽트 상태 바이트를 보유합니다
    uint8_t devStatus; // 각 장치 작업 후 반환 상태 (0 = success, !0 = error)
    uint16_t packetSize; // 예상 DMP 패킷 크기 (default is 42 bytes)
    uint16_t fifoCount; // 현재 FIFO에 있는 모든 바이트 수
    uint8_t fifoBuffer[64]; // FIFO 저장 버퍼

    // orientation/motion vars
    Quaternion q; // [w, x, y, z] quaternion container
    VectorFloat gravity; // [x, y, z] gravity vector
    float ypr[3]; // [yaw, pitch, roll] yaw/pitch/roll container and gravity vector

    VectorInt16 aa;         // [x, y, z]            accel sensor measurements
    VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
    VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
    float euler[3]; 

    //PID
    double originalSetpoint = 180.0;  //최적의 평행각도
    double setpoint = originalSetpoint;
    double movingAngleOffset = 0.3;
    double input, output;

    //PID 값을 자신의 움직임에 맞게 조금씩 조절한다
    double Kp = 50;   //60
    double Kd = 2.2; //1.5
    double Ki = 290; //200
    PID pid(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);

    double motorSpeedFactorLeft = 0.6;  //이 값들도 수동으로 봐 가면서 조금씩 조절한다
    double motorSpeedFactorRight = 0.5;

    SelfMotor SelfMotorContol(motorSpeedFactorLeft, motorSpeedFactorRight);

    volatile bool mpuInterrupt = false; // MPU 인터럽트 핀이 하이 상태가 되었는지 여부를 나타냅니다
    void dmpDataReady()
    {
        mpuInterrupt = true;
    }


    void setup()
    {
          // join I2C bus (I2Cdev library doesn't do this automatically)
          #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
          Wire.begin();
          Wire.setClock(400000); 
          //TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz) 원래 코드는  Wire.setClock(400000); //
          #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
          Fastwire::setup(400, true);
          #endif

          Serial.begin(115200);
          while (!Serial);

          mpu.initialize();

          devStatus = mpu.dmpInitialize();

          // IMU_zero 샘플에서 얻어진 값으로 입력 해준다
          mpu.setXGyroOffset(120);
          mpu.setYGyroOffset(-85);
          mpu.setZGyroOffset(-23);
          mpu.setZAccelOffset(1137); // 1688 factory default for my test chip

          // 확실히 동작하는지 확인(returns 0 if so)
          if (devStatus == 0)
          {
                // DMP 전원을 켭니다. 준비가 되었으므로
                mpu.setDMPEnabled(true);

                // 아두이노 인터럽트 탐지 활성화
                attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
                mpuIntStatus = mpu.getIntStatus();

                // 메인 루프() 함수가 사용해도 괜찮다는 것을 알 수 있도록 DMP Ready 플래그를 설정합니다
                dmpReady = true;

                // 나중에 비교하기 위해 예상 DMP 패킷 크기 가져오기
                packetSize = mpu.dmpGetFIFOPacketSize();
                
                //setup PID
                pid.SetMode(AUTOMATIC);
                pid.SetSampleTime(10);
                pid.SetOutputLimits(-255, 255); 
          }
          else
          {
                // ERROR!
                // 1 = initial memory load failed
                // 2 = DMP configuration updates failed
                // (if it's going to break, usually the code will be 1)
                Serial.print(F("DMP Initialization failed (code "));
                Serial.print(devStatus);
                Serial.println(F(")"));
          }


    }


    void loop()
    {
            // if programming failed, don't try to do anything
            if (!dmpReady) return;

            // wait for MPU interrupt or extra packet(s) available
            while (!mpuInterrupt && fifoCount < packetSize)
            {
                      //no mpu data - performing PID calculations and output to motors 
                      pid.Compute();
                      //SelfMotorContol.move(output, MIN_ABS_SPEED);//move(int leftSpeed, int rightSpeed, int minAbsSpeed)
                      
            }

            // reset interrupt flag and get INT_STATUS byte
            mpuInterrupt = false;
            mpuIntStatus = mpu.getIntStatus();

            // get current FIFO count
            fifoCount = mpu.getFIFOCount();

            // check for overflow (this should never happen unless our code is too inefficient)
            if ((mpuIntStatus & 0x10) || fifoCount == 1024)
            {
            // reset so we can continue cleanly
                mpu.resetFIFO();
                Serial.println(F("FIFO overflow!"));

            // otherwise, check for DMP data ready interrupt (this should happen frequently)
            }
            else if (mpuIntStatus & 0x02)
            {
              // wait for correct available data length, should be a VERY short wait
                while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

                // read a packet from FIFO
                mpu.getFIFOBytes(fifoBuffer, packetSize);
                
                // track FIFO count here in case there is > 1 packet available
                // (this lets us immediately read more without waiting for an interrupt)
                fifoCount -= packetSize;

                mpu.dmpGetQuaternion(&q, fifoBuffer);
                mpu.dmpGetGravity(&gravity, &q);
                mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
                input = ypr[1] * 180/M_PI + 180;  //피치값만 가지고???
                
                //디버깅용
                Serial.println(ypr[2] * 180/M_PI);
            }
    }
